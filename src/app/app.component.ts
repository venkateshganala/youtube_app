import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 
  private id1:string ='ZWJH7JQCjLM';
  id2='gfkTfcpWqAY';
  id3='tCAt8eEKPDc';
  id4='mUxS-35qO44';
  id5='Fkd9TWUtFm0';
  id6='3FvSLA-Kvvs';
  id7='e_TxH59MclA';
  id8='egQH4vM1q1A';
  id = 'qDuKsiwS5xw';
  playerVars = {
    cc_lang_pref: 'en'
  };
  private player;
  private ytEvent;

  constructor() {}
  onStateChange(event) {
    this.ytEvent = event.data;
  }
  savePlayer(player) {
    this.player = player;
  }

  playVideo() {
    this.player.playVideo();
  }

  pauseVideo() {
    this.player.pauseVideo();
  }
}
